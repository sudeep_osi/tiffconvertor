﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using Syncfusion.Pdf;
using Syncfusion.Pdf.Graphics;

namespace Tif2Pdf
{
    class Program
    {
        public class File
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string Path { get; set; }

        }
        static void Main(string[] args)
        {
            string conString = string.Empty, table = string.Empty, idColumn=string.Empty, nameColumn=string.Empty, pathColumn=string.Empty;

            Console.WriteLine("Please enter connection string in the below format:");
            Console.WriteLine("Data Source=MEDUATSQL01.CanyonMedicalBilling.com;User ID=sysadmin1;Password=M0v3D0cs!@#;Initial Catalog=CIMI_Live\n");
            conString = Console.ReadLine();

           TABLE: Console.WriteLine("\nPlease enter table name");
            table = Console.ReadLine();
            if (string.IsNullOrEmpty(table))
                goto TABLE;

            UID: Console.WriteLine("\nPlease enter unique id Column name");
            idColumn = Console.ReadLine();
            if (string.IsNullOrEmpty(idColumn))
                goto UID;

            NAME: Console.WriteLine("\nPlease enter file column name");
            nameColumn = Console.ReadLine();
            if (string.IsNullOrEmpty(nameColumn))
                goto NAME;

            PATH: Console.WriteLine("\nPlease enter path column name");
            pathColumn = Console.ReadLine();
            if (string.IsNullOrEmpty(pathColumn))
                goto PATH;

            Console.WriteLine("\nProcessing please wait.....");

            var data = getData(conString, table, idColumn, nameColumn, pathColumn);

            if (data != null && data.Count > 0)
            {
                foreach (var file in data)
                {
                    if (!string.IsNullOrEmpty(file.Path))
                    {
                        try
                        {
                            //Create a PDF document
                            PdfDocument doc = new PdfDocument();
                            //Setting margin size of all the pages to zero
                            doc.PageSettings.Margins.All = 0;
                            //Get the image stream and draw frame by frame
                            using (var tiffImage = new PdfBitmap(file.Path))
                            {
                                int frameCount = tiffImage.FrameCount;
                                for (int i = 0; i < frameCount; i++)
                                {
                                    //Add pages to the document
                                    var page = doc.Pages.Add();
                                    //Getting page size to fit the image within the page
                                    SizeF pageSize = page.GetClientSize();
                                    //Selecting frame in TIFF
                                    tiffImage.ActiveFrame = i;
                                    //Draw TIFF frame
                                    page.Graphics.DrawImage(tiffImage, 0, 0, pageSize.Width, pageSize.Height);
                                }
                            }
                            var path = file.Path.Replace(".tiff", ".pdf");
                            //Saves the document
                            doc.Save(path);
                            //Close the document
                            doc.Close(true);

                            int count = updateData(conString, table, idColumn, nameColumn, pathColumn, file.Name.Replace(".tiff", ".pdf"), path, file.Id);
                            if (count > 0)
                                Console.WriteLine("Success :  " + file.Name);
                            else
                                Console.WriteLine("Failure :  " + file.Name);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("Error occured while processing the File :  " + file.Name);
                        }
                    }
                    else
                    {
                        Console.WriteLine("Failure :  Path is not available for " + file.Name);
                    }
                }
            }
            else
            {
                Console.WriteLine("\nData is not available to process");
            }
            Console.ReadKey();
        }

        static List<File> getData(string connectionString, string table, string idCol, string nameCol, string pathCol)
        {
            List<File> data = new List<File>();
            StringBuilder query = new StringBuilder();
            query.AppendLine(string.Format("SELECT  {0}, {1}, {2} FROM {3} Where {4} like '%tiff'", idCol, nameCol, pathCol, table, nameCol));

            // create connection and command
            using (SqlConnection cn = new SqlConnection(connectionString))
            using (SqlCommand cmd = new SqlCommand(query.ToString(), cn))
            {

                // open connection, execute SELECT, close connection
                cn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    File file = new File();
                    file.Id = Convert.ToInt32(reader[idCol]);
                    file.Name = reader[nameCol].ToString();
                    file.Path = reader[pathCol].ToString();

                    data.Add(file);
                }
                reader.Close();
                cn.Close();
            }

            return data;
        }

        static int updateData(string connectionString, string table, string idCol, string nameCol, string pathCol, string fileName, string path, int id)
        {
            int result = 0;
            StringBuilder query = new StringBuilder();
            query.AppendLine(string.Format("Update {0} Set {1} = '{2}', {3} = '{4}' Where {5} = {6}", table, nameCol, fileName, pathCol, path, idCol, id));

            // create connection and command
            using (SqlConnection cn = new SqlConnection(connectionString))
            using (SqlCommand cmd = new SqlCommand(query.ToString(), cn))
            {

                // open connection, execute SELECT, close connection
                cn.Open();
                result = cmd.ExecuteNonQuery();
                cn.Close();
            }

            return result;
        }
    }

    
}
